package org.acme;


import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;

@QuarkusTest
public class JavaToXMLTest {



    @Test
    public void test() {

        Shiporder shiporder = new Shiporder();
        shiporder.setOrderperson("John Smith");
        shiporder.setOrderid("889923");

        Shiporder.Shipto shipto1 = new Shiporder.Shipto();
        shipto1.setName("Ola Nordmann");
        shipto1.setAddress("Langgt 23");
        shipto1.setCity("4000 Stavange");
        shipto1.setCountry("Norway");

        shiporder.setShipto(shipto1);

        Shiporder.Item item1 = new Shiporder.Item();
        item1.setTitle("Empire Burlesque");
        item1.setNote("Special Edition");
        item1.setQuantity(new BigInteger("1"));
        item1.setPrice(new BigDecimal("10.90"));

        shiporder.addItem(item1);
        Shiporder.Item item2 = new Shiporder.Item();
        item2.setTitle("Hide your heart");
        item2.setQuantity(new BigInteger("1"));
        item2.setPrice(new BigDecimal("9.90"));

        shiporder.addItem(item2);

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(Shiporder.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            //Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            //Print XML String to Console
            StringWriter sw = new StringWriter();
            //Write XML to StringWriter
            jaxbMarshaller.marshal(shiporder, sw);
            //Verify XML Content
            String xmlContent = sw.toString();
            System.out.println( xmlContent );

        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }
}
