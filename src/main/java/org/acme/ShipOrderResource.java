package org.acme;

import org.acme.service.ShipOrderService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import java.math.BigDecimal;
import java.math.BigInteger;

@Path("/ship-order")
public class ShipOrderResource {


    @Inject
    ShipOrderService shipOrderService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String shipOrderToXML() {
        Shiporder shiporder = new Shiporder();
        shiporder.setOrderperson("John Smith");
        shiporder.setOrderid("889923");

        Shiporder.Shipto shipto1 = new Shiporder.Shipto();
        shipto1.setName("Ola Nordmann");
        shipto1.setAddress("Langgt 23");
        shipto1.setCity("4000 Stavange");
        shipto1.setCountry("Norway");

        shiporder.setShipto(shipto1);

        Shiporder.Item item1 = new Shiporder.Item();
        item1.setTitle("Empire Burlesque");
        item1.setNote("Special Edition");
        item1.setQuantity(new BigInteger("1"));
        item1.setPrice(new BigDecimal("10.90"));

        shiporder.addItem(item1);
        Shiporder.Item item2 = new Shiporder.Item();
        item2.setTitle("Hide your heart");
        item2.setQuantity(new BigInteger("1"));
        item2.setPrice(new BigDecimal("9.90"));

        shiporder.addItem(item2);


        return this.shipOrderService.shipOrderToXML(shiporder, Shiporder.class);
    }
}
